**Controls:**
 - Movement: WASD
 - Shoot: Left click mouse
 - Reload: R
 - Melee: F
 - Dash: Space
 - Change weapon: mouse wheel or XC
 - Change minimap scale: Tab
 - Settings: Esc
 - Buy item: E
 - Use item: Right click mouse

**Cosas importantes**
 - Pasas al siguiente nivel cuando has superado todas las salas.
 - A veces aparecen enemigos en las paredes, empezó siendo un bug pero me ha gustado y lo he dejado como feature (te fuerza a acercarte a ellos y matarlos con el melee muchas veces, cumplen un poco también el papel de los snipers obligándote a no campear)
 - No hay feedback visual. Si el personaje se queda totalmente quieto es porque un enemigo te ha stuneado con un lanzallamas. Si te envían a tomar por culo es porque o un kamikaze o una granada han expotado cerca tuya.
 - El juego tiene bugs, especialmente en el cambio de escenas. Happens, podría solucionarlos pero la verdad me apetece descansar una poca más.

 **Consejos (yo creo que el resultado es muy balanced y cumple muy bien su objetivo, pero puedes mirar este punto si ves el juego más caótico de lo que te gustaría, creo que jugando se pueden aprender estas cosas pero por si acaso, como no tengo tutoriales, dejo esto por aquí)**
 - El dash es bueno contra los enemigos con escopeta.
 - Mantente en movimiento para que no te pillen los enemigos kamikazes.
 - Moverse en círculos alrededor de los enemigos con rifle de asalto.
 - Los enemigos con grenade launcher son useless, ignorálos.
 - Si ves a alguien con rifle de francotirador, déjalo todo y ves a por él.
 - El juego está pensado para rushearse, sacar la máxima score posible y masterizar la gestión de los enemigos (de ahí el sistema de la tienda, por ejemplo), pero si te da igual la score se puede jugar del chill.
 - El stun flamethrower viene genial para acercarse a los enemigos con armas con mucha cadencia subidos a las paredes (o metidos en ellas).
 - No uses el grenade launcher (la peor arma con diferencia).
