using UnityEngine;

[CreateAssetMenu(fileName = "LevelScriptableObject", menuName = "Scriptable Objects/Levels/Level", order = 1)]
public class LevelsSO : ScriptableObject
{
    public GameObject levelObject;
    public LevelsListSO.AvailableDoorSpawn[] whereAreLevelsNeeded;
}
