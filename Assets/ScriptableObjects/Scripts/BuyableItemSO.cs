using UnityEngine;

public abstract class BuyableItemSO : ScriptableObject
{
    public Sprite image;
    public string itemName;
    [TextArea]
    public string description;
    public int cost;

    public abstract void ItemAction();
}
