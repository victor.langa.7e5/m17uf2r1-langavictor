using UnityEngine;

[CreateAssetMenu(fileName = "EnemyScriptableObject", menuName = "Scriptable Objects/Entities/Enemy", order = 2)]
public class EnemySO : CharacterSO
{
    public float damagePerContact;

    public int scoreValue;

    public int probabilityToDropItem;
    public ItemListSO dropableItemsList;
}
