using UnityEngine;

[CreateAssetMenu(fileName = "ItemListScriptableObject", menuName = "Scriptable Objects/Items/ItemList", order = 0)]
public class ItemListSO : ScriptableObject
{
    public GameObject[] items;
}
