using UnityEngine;

[CreateAssetMenu(fileName = "WeaponScriptableObject", menuName = "Scriptable Objects/Weapons/Weapon", order = 1)]
public class WeaponSO : ScriptableObject
{
    public GameObject bullet;

    public float reloadTime;
    public float timeBetweenShots;

    public int bulletsStored;
    public int totalBullets;
    public int bulletsLeft;

    public virtual void SetValues(WeaponSO weaponSO)
    {
        bullet = weaponSO.bullet;

        reloadTime = weaponSO.reloadTime;
        timeBetweenShots = weaponSO.timeBetweenShots;

        bulletsStored = weaponSO.bulletsStored;
        totalBullets = weaponSO.totalBullets;
        bulletsLeft = weaponSO.bulletsLeft;
    }
}
