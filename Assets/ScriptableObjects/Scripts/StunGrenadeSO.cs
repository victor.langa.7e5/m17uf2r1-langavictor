using UnityEngine;

[CreateAssetMenu(fileName = "StunGrenadeItemScriptableObject", menuName = "Scriptable Objects/Items/StunGrenadeItem", order = 2)]

public class StunGrenadeSO : BuyableItemSO
{
    public float timeToStun;
    public float stunRadius;

    public override void ItemAction()
    {
        foreach (RaycastHit2D ray in Physics2D.CircleCastAll(GameManager.Instance.GetPlayerPosition(), stunRadius, Vector2.zero))
            if (ray.collider.gameObject.CompareTag("Enemy"))
                    ray.collider.gameObject.GetComponent<Enemy>().ApplyStun(timeToStun);
    }


}
