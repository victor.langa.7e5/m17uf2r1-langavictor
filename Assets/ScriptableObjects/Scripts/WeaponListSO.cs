using UnityEngine;

[CreateAssetMenu(fileName = "WeaponListScriptableObject", menuName = "Scriptable Objects/Weapons/WeaponList", order = 0)]
public class WeaponListSO : ScriptableObject
{
    public WeaponSO pistolValues;
    public ShotgunSO shotgunValues;
    public WeaponSO assaultRifleValues;
    public WeaponSO grenadeLauncherValues;
    public WeaponSO sniperRifleValues;
    public StunFlamethrowerSO stunFlamethrowerValues;
}
