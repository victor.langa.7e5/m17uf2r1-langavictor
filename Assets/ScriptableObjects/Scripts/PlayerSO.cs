using UnityEngine;

[CreateAssetMenu(fileName = "PlayerScriptableObject", menuName = "Scriptable Objects/Entities/Player", order = 1)]
public class PlayerSO : CharacterSO
{
    public float invulnerabilityTimeDamage;
    public float invulnerabilityTimeDash;

    public int dashForce;
    public float dashCooldown;

    public float meleeDamage;
    public float meleeRange;
    public float meleeForce;
    public float meleeCooldown;
}
