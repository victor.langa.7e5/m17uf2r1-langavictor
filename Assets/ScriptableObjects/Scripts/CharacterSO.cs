using UnityEngine;

[CreateAssetMenu(fileName = "CharacterScriptableObject", menuName = "Scriptable Objects/Entities/Character", order = 0)]
public class CharacterSO : ScriptableObject
{
    public float maxHp;
    public float hpLeft;

    public float speed;
}
