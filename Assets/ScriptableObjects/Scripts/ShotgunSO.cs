using UnityEngine;

[CreateAssetMenu(fileName = "ShotgunScriptableObject", menuName = "Scriptable Objects/Weapons/Shotgun", order = 2)]
public class ShotgunSO : WeaponSO
{
    public int bulletsPerShot;

    public override void SetValues(WeaponSO weaponSO)
    {
        base.SetValues(weaponSO);
        bulletsPerShot = (weaponSO as ShotgunSO).bulletsPerShot;
    }
}
