using UnityEngine;

[CreateAssetMenu(fileName = "ExplosionScriptableObject", menuName = "Scriptable Objects/Actions/Explosion", order = 0)]
public class ExplosionSO : ScriptableObject
{
    public float explosionForce;
    public float explosionDamage;

    public float minimumDistanceToExplode;
    public float explosionRadius;

    public float spareSeconds;
    public float explosionCooldown;
}
