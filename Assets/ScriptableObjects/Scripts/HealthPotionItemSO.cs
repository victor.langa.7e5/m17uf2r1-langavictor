using UnityEngine;

[CreateAssetMenu(fileName = "HealthPotionItemScriptableObject", menuName = "Scriptable Objects/Items/HealthPotionItem", order = 1)]

public class HealthPotionItemSO : BuyableItemSO
{
    public float hpToHeal;

    public override void ItemAction() => GameObject.Find("PlayerTest").GetComponent<Player>().HealHp(hpToHeal / 2);
}
