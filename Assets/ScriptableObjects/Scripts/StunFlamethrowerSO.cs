using UnityEngine;

[CreateAssetMenu(fileName = "StunFlamethrowerScriptableObject", menuName = "Scriptable Objects/Weapons/StunFlamethrower", order = 3)]
public class StunFlamethrowerSO : WeaponSO
{
    public float timeToStun;

    public override void SetValues(WeaponSO weaponSO)
    {
        base.SetValues(weaponSO);
        timeToStun = (weaponSO as StunFlamethrowerSO).timeToStun;
    }
}
