using UnityEngine;

[CreateAssetMenu(fileName = "EnemyListScriptableObject", menuName = "Scriptable Objects/Entities/EnemyList", order = 3)]
public class EnemyListSO : ScriptableObject
{
    public GameObject[] enemies;
}
