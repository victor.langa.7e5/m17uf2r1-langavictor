using UnityEngine;

[CreateAssetMenu(fileName = "LevelScriptableObject", menuName = "Scriptable Objects/Levels/LevelsList", order = 2)]
public class LevelsListSO : ScriptableObject
{
    public enum AvailableDoorSpawn
    { 
        LeftDoor,
        RightDoor,
        TopDoor,
        DownDoor,
        None
    }

    public LevelsSO[] levels;
}
