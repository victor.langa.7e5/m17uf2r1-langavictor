using System.Collections.Generic;
using UnityEngine;

public class LevelsSpawner : MonoBehaviour
{
    private static LevelsSpawner _instance;
    public static LevelsSpawner Instance { get { return _instance; } }

    [SerializeField] private LevelsListSO _levelsListSO;
    [SerializeField] private int _spawnSeparation;

    [SerializeField] private int[] _roomsPerFloor;
    private int _roomsSpawned;

    private List<Spawner> _spawners = new List<Spawner>();
    [SerializeField] private GameObject _initialDoors;
    public delegate void DespawnDoors();
    public event DespawnDoors OnDoorDespawn;

    void Awake()
    {
        if (_instance != this && _instance != null) Destroy(this);
        _instance = this;
    }

    void Start()
    {
        _roomsSpawned = 0;
        GenerateRooms(_levelsListSO.levels[0], Vector2.zero, LevelsListSO.AvailableDoorSpawn.None);
        Destroy(_initialDoors);
        OnDoorDespawn.Invoke();
    }

    void GenerateRooms(LevelsSO currentRoom, Vector2 position, LevelsListSO.AvailableDoorSpawn doorToIgnore)
    {
        Vector2 newPosition;
        LevelsListSO.AvailableDoorSpawn doorToIgnoreInNextSpawnedRoom;

        for (int i = 0; i < currentRoom.whereAreLevelsNeeded.Length; i++)
        {
            if (currentRoom.whereAreLevelsNeeded[i] == doorToIgnore)
                continue;

            switch (currentRoom.whereAreLevelsNeeded[i])
            {
                case LevelsListSO.AvailableDoorSpawn.TopDoor:
                    newPosition = new Vector2(position.x, position.y + _spawnSeparation);
                    doorToIgnoreInNextSpawnedRoom = LevelsListSO.AvailableDoorSpawn.DownDoor;
                    break;
                case LevelsListSO.AvailableDoorSpawn.DownDoor:
                    newPosition = new Vector2(position.x, position.y - _spawnSeparation);
                    doorToIgnoreInNextSpawnedRoom = LevelsListSO.AvailableDoorSpawn.TopDoor;
                    break;
                case LevelsListSO.AvailableDoorSpawn.LeftDoor:
                    newPosition = new Vector2(position.x - _spawnSeparation, position.y);
                    doorToIgnoreInNextSpawnedRoom = LevelsListSO.AvailableDoorSpawn.RightDoor;
                    break;
                case LevelsListSO.AvailableDoorSpawn.RightDoor:
                    newPosition = new Vector2(position.x + _spawnSeparation, position.y);
                    doorToIgnoreInNextSpawnedRoom = LevelsListSO.AvailableDoorSpawn.LeftDoor;
                    break;
                default:
                    continue;
            }

            LevelsSO roomToInstantiate = FindCompatibleRoom(newPosition);
            _spawners.Add(Instantiate(roomToInstantiate.levelObject, newPosition, roomToInstantiate.levelObject.transform.localRotation).GetComponentInChildren<Spawner>());
            _roomsSpawned++;

            if (currentRoom.whereAreLevelsNeeded.Length > 1 && Physics2D.OverlapPoint(newPosition) == null)
               GenerateRooms(roomToInstantiate, newPosition, doorToIgnoreInNextSpawnedRoom);
        }
    }

    LevelsSO FindCompatibleRoom(Vector2 newPosition)
    {
        List<LevelsSO> compatibleRooms = new();
        List<LevelsListSO.AvailableDoorSpawn> doorsToAttach = new();

        // Loop 1: check top room down door
        // Loop 2: check down room top door
        // Loop 3: check left room right door door
        // Loop 4: check right room left door

        for (int i = 0; i < 4; i++)
        {
            Vector2 rayOrigin = new();
            switch (i)
            {
                case 0:
                    rayOrigin = new Vector2(newPosition.x, newPosition.y + _spawnSeparation / 2 + 0.5f);
                    break;
                case 1:
                    rayOrigin = new Vector2(newPosition.x, newPosition.y - _spawnSeparation / 2 - 0.5f);
                    break;
                case 2:
                    rayOrigin = new Vector2(newPosition.x - _spawnSeparation / 2 - 0.5f, newPosition.y);
                    break;
                case 3:
                    rayOrigin = new Vector2(newPosition.x + _spawnSeparation / 2 + 0.5f, newPosition.y);
                    break;
            };

            Collider2D collider = Physics2D.OverlapPoint(rayOrigin);

            if (collider != null && collider.gameObject.name == "Door")
                switch (i)
                {
                    case 0:
                        doorsToAttach.Add(LevelsListSO.AvailableDoorSpawn.TopDoor);
                        break;
                    case 1:
                        doorsToAttach.Add(LevelsListSO.AvailableDoorSpawn.DownDoor);
                        break;
                    case 2:
                        doorsToAttach.Add(LevelsListSO.AvailableDoorSpawn.LeftDoor);
                        break;
                    case 3:
                        doorsToAttach.Add(LevelsListSO.AvailableDoorSpawn.RightDoor);
                        break;
                };
        }

        for (int i = 0; i < _levelsListSO.levels.Length; i++)
        {
            if (_roomsSpawned < _roomsPerFloor[GameManager.Instance.CurrentFloor - 1])
            {
                bool isCompatibleRoom = true;

                foreach (var neededDoor in doorsToAttach)
                {
                    bool hasToContinue = false;
                    foreach (var door in _levelsListSO.levels[i].whereAreLevelsNeeded)
                        if (door == neededDoor)
                        {
                            hasToContinue = true;
                            break;
                        }

                    if (!hasToContinue)
                        isCompatibleRoom = false;
                }

                if (isCompatibleRoom)
                    compatibleRooms.Add(_levelsListSO.levels[i]);
            }
            else
                try
                {
                    switch (doorsToAttach[0])
                    {
                        case LevelsListSO.AvailableDoorSpawn.TopDoor:
                            return _levelsListSO.levels[1];
                        case LevelsListSO.AvailableDoorSpawn.DownDoor:
                            return _levelsListSO.levels[2];
                        case LevelsListSO.AvailableDoorSpawn.LeftDoor:
                            return _levelsListSO.levels[3];
                        case LevelsListSO.AvailableDoorSpawn.RightDoor:
                            return _levelsListSO.levels[4];
                    }
                }
                catch 
                {
                    Debug.Log("Game scene loaded to prevent errors");
                    ProjectSceneManager.LoadGameNotAsync(); 
                }
        }

        return compatibleRooms[Random.Range(0, compatibleRooms.Count)];
    }

    public int GetRoomsSpawned() => _roomsSpawned;
}
