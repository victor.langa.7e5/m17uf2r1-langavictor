using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private EnemyListSO _enemyList;
    [SerializeField] private GameObject _ground;

    [SerializeField] private GameObject _minimapFeedback;

    [SerializeField] private int _waves;
    [SerializeField] private int _enemiesPerWave;

    private GameObject[] _enemiesSpawned;

    public delegate void ChangeDoorState(bool isOpenDoor);
    public event ChangeDoorState OnChangeState;

    private void Awake() => LevelsSpawner.Instance.OnDoorDespawn += DisableDoors;

    void Start()
    {
        _enemiesSpawned = new GameObject[_enemiesPerWave];
        _waves = Random.Range(1, _waves);
    }

    private void OnEnable() => OnChangeState.Invoke(true);

    void Update()
    {
        switch (_waves > 0, CheckIfEnemiesHaveBeenDefeated())
        {
            case (true, true):
                SpawnEnemies();
                GameManager.Instance.AddWaveScore();
                break;
            case (false, true):
                GameManager.Instance.AddEndRoomScore();
                GameManager.Instance.AddRoomSurpassed();
                OnChangeState.Invoke(false);
                Instantiate(_minimapFeedback, transform.position, _minimapFeedback.transform.rotation);
                Destroy(gameObject);
                break;
        }
    }

    private void OnDestroy()
    {
        LevelsSpawner.Instance.OnDoorDespawn -= DisableDoors;
    }

    Vector2 GetValidPositionToSpawn()
    {
        Vector2 spawnPosition;

        do
        {
            spawnPosition = new Vector2(
                _ground.transform.position.x + Random.Range(-(_ground.transform.localScale.x / 2 - _ground.transform.localScale.x / 10), _ground.transform.localScale.x / 2 - _ground.transform.localScale.x / 10),
                _ground.transform.position.y + Random.Range(-(_ground.transform.localScale.y / 2 - _ground.transform.localScale.y / 10), _ground.transform.localScale.y / 2 - _ground.transform.localScale.y / 10)
                );
        } while (Physics2D.OverlapPoint(spawnPosition) != null && Vector2.Distance(spawnPosition, GameManager.Instance.GetPlayerPosition()) > 3);

        return spawnPosition;
    }

    void SpawnEnemies()
    {
        _waves--;
        int enemiesPerWaveRange = Random.Range(_enemiesPerWave - 1, _enemiesPerWave + 1);
        for (int i = 0; i < enemiesPerWaveRange; i++)
        {
            GameObject enemy = _enemyList.enemies[Random.Range(0, _enemyList.enemies.Length)];
            _enemiesSpawned[i] = Instantiate(enemy, GetValidPositionToSpawn(), enemy.transform.rotation);
        }
    }

    bool CheckIfEnemiesHaveBeenDefeated()
    {
        foreach (GameObject enemy in _enemiesSpawned)
            if (enemy != null)
                return false;
        return true;
    }

    public void DisableDoors()
    {
        OnChangeState.Invoke(false);
    }
}
