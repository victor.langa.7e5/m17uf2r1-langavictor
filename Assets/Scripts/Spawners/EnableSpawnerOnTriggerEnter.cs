using UnityEngine;

public class EnableSpawnerOnTriggerEnter : MonoBehaviour
{
    [SerializeField] Spawner _spawner;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            _spawner.enabled = true;
            Destroy(gameObject);
        }
    }
}
