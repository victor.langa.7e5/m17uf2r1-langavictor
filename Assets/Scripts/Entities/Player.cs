using System.Collections;
using UnityEngine;

public class Player : Character, IStunable
{
    private Collider2D _collider;

    [SerializeField] private PlayerSO _playerSO;
    private Dash _dash;
    private Melee _melee;

    private bool _canBeDamaged;
    private float _invulnerabilityTimeDamage;
    private float _invulnerabilityTimeDash;

    private int _dashForce;
    private float _dashCooldown;

    private float _meleeDamage;
    private float _meleeRange;
    private float _meleeForce;
    private float _meleeCooldown;

    public IStunable.StunStateEnum StunState { get; set; }

    public float StunTime { get; set; }

    private void Awake()
    {
        maxHp = _playerSO.maxHp;
        hpLeft = _playerSO.hpLeft;

        speed = _playerSO.speed;

        _invulnerabilityTimeDamage = _playerSO.invulnerabilityTimeDamage;
        _invulnerabilityTimeDash = _playerSO.invulnerabilityTimeDash;

        _dashForce = _playerSO.dashForce;
        _dashCooldown = _playerSO.dashCooldown;

        _meleeDamage = _playerSO.meleeDamage;
        _meleeRange = _playerSO.meleeRange;
        _meleeForce = _playerSO.meleeForce;
        _meleeCooldown = _playerSO.meleeCooldown;
    }

    void Start()
    {
        _collider = GetComponent<Collider2D>();

        _dash = GetComponent<Dash>();
        _melee = GetComponent<Melee>();

        _canBeDamaged = true;
    }

    void Update()
    {
        if (!IsStunned())
        {
            aimTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            _melee.MeleeAttack(_collider.bounds.ClosestPoint((Vector2)aimTarget), aimTarget, _collider.bounds.size, _meleeDamage, _meleeRange, _meleeForce, _meleeCooldown);

            if (_dash.TryDash(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")), _dashForce, _dashCooldown))
                StartCoroutine(Invulnerability(_invulnerabilityTimeDash));
        }
    }

    public override void DoDamage(float damage)
    {
        if (!_canBeDamaged)
            return;

        StartCoroutine(Invulnerability(_invulnerabilityTimeDamage));
        base.DoDamage(damage);

        if (hpLeft <= 0) 
            _canBeDamaged = false;
    }

    IEnumerator Invulnerability(float invulnerabilityTime)
    {
        _canBeDamaged = false;
        yield return new WaitForSecondsRealtime(invulnerabilityTime);
        _canBeDamaged = true;
    }

    public bool IsStunned()
    {
        if (StunTime > 0)
        {
            StunTime -= Time.deltaTime;
            return true;
        }

        if (StunState == IStunable.StunStateEnum.Stunned)
            EndStun();

        return false;
    }

    public void ApplyStun(float stunTime)
    {
        if (StunState == IStunable.StunStateEnum.NotStunned)
        { 
            StunState = IStunable.StunStateEnum.Stunned;
            StunTime = stunTime;
            ChangeStunState(false);
        }
    }

    public void EndStun() => StartCoroutine(StartInvulnerabilityToStun());

    private IEnumerator StartInvulnerabilityToStun()
    {
        StunState = IStunable.StunStateEnum.InvulnerableToStun;
        yield return new WaitForSeconds(1);
        StunState = IStunable.StunStateEnum.NotStunned;
        ChangeStunState(true);
    }
    private void ChangeStunState(bool areComponentsEnabled)
    {
        foreach (var component in gameObject.GetComponentsInChildren<MonoBehaviour>())
            if (!ReferenceEquals(this, component))
                component.enabled = areComponentsEnabled;
    }

}
