using Mono.Cecil;
using UnityEngine;

public class Enemy : Character, IDropable, IStunable
{
    [SerializeField] private EnemySO _enemySO;

    private float _damagePerContact;

    private int _scoreValue;

    private int _probabilityToDropItem;
    public GameObject[] DropableItems { get; set; }
    public float StunTime { get; set; }
    public IStunable.StunStateEnum StunState { get; set; }

    void Start()
    {
        StunState = IStunable.StunStateEnum.NotStunned;
        StunTime = 0;

        maxHp = _enemySO.maxHp;
        hpLeft = _enemySO.hpLeft;

        speed = _enemySO.speed;

        _damagePerContact = _enemySO.damagePerContact;

        _scoreValue = _enemySO.scoreValue;

        _probabilityToDropItem = _enemySO.probabilityToDropItem;
        DropableItems = _enemySO.dropableItemsList.items;
    }

    void Update()
    {
        if (!IsStunned())
            aimTarget = GameManager.Instance.GetPlayerPosition();
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            collision.gameObject.GetComponent<Player>().DoDamage(_damagePerContact);
    }

    protected override void Die()
    {
        GameManager.Instance.AddScore(_scoreValue);
        GameManager.Instance.enemiesKilled += 1;
        base.Die(); 
        
        if (Random.Range(0, 100) < _probabilityToDropItem)
        {
            GameObject objectToInstantiate = ((IDropable)this).ReturnItemToDrop();
            Instantiate(objectToInstantiate, transform.position, objectToInstantiate.transform.rotation);
        }
    }

    public bool IsStunned()
    {
        if (StunTime > 0)
        {
            StunTime -= Time.deltaTime;
            return true;
        }

        if (StunState == IStunable.StunStateEnum.Stunned)
            EndStun();

        return false;
    }

    public void ApplyStun(float stunTime)
    {
        StunTime = stunTime;
        ChangeStunState(false);
        StunState = IStunable.StunStateEnum.Stunned;
    }

    public void EndStun()
    {
        ChangeStunState(true);
        StunState = IStunable.StunStateEnum.NotStunned;
    }

    private void ChangeStunState(bool areComponentsEnabled)
    {
        foreach (var component in gameObject.GetComponentsInChildren<MonoBehaviour>())
            if (!ReferenceEquals(this, component)) 
                component.enabled = areComponentsEnabled;
    }
}
