using UnityEngine;

public class DestructibleObject : MonoBehaviour, IDropable
{
    [SerializeField] private float _probabilityToDropItem;
    [SerializeField] private ItemListSO _dropableItems;
    public GameObject[] DropableItems { get; set; }

    void Start() => DropableItems = _dropableItems.items;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            if (Random.Range(0, 100) < _probabilityToDropItem)
            {
                GameObject objectToInstantiate = ((IDropable)this).ReturnItemToDrop();
                Instantiate(objectToInstantiate, transform.position, objectToInstantiate.transform.rotation);
            }
            Destroy(gameObject);
        }
    }
}
