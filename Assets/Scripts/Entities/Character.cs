using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private Aim _aim;
    protected Vector3 aimTarget;

    protected float maxHp;
    protected float hpLeft;

    protected float speed;

    void Awake() => hpLeft = maxHp;

    void LateUpdate() => _aim.AimAt(aimTarget);

    public Vector3 GetAimTarget() => aimTarget;

    public float GetMaxHp() => maxHp;

    public float GetHpLeft() => hpLeft;

    public float GetSpeed() => speed;

    public virtual void DoDamage(float damage)
    {
        hpLeft -= damage;
        if (hpLeft <= 0)
            Die();
    }

    protected virtual void Die()
    {
        if (gameObject.CompareTag("Player"))
            StartCoroutine(GameManager.Instance.EndGame(false));
        else
            Destroy(gameObject);
    }

    public virtual void HealHp(float hpToHeal)
    {
        if (hpLeft + hpToHeal > maxHp)
            hpLeft = maxHp;
        else
            hpLeft += hpToHeal; 
    }
}
