using UnityEngine;

public class Doors : MonoBehaviour
{
    [SerializeField] private Spawner _spawner;

    private void Awake() => _spawner.OnChangeState += ChangeDoorState;

    private void OnDisable() => _spawner.OnChangeState -= ChangeDoorState;

    public void ChangeDoorState(bool isOpenDoor)
    {
        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).gameObject.SetActive(isOpenDoor);
    }
}
