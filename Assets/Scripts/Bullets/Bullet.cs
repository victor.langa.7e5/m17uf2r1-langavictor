using Unity.VisualScripting;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] protected float bulletDamage;
    [SerializeField] protected float bulletSpeed;

    private Rigidbody2D _rb;

    protected Vector2 _direction;

    void Awake()
    {
        _direction = transform.parent.GetComponent<WeaponContext>().weapon.character.GetAimTarget() - transform.parent.position;
        _direction.Normalize();
        transform.parent = null;
    }

    private void Update()
    {
        if (_rb.velocity.magnitude < 1)
            Destroy(this);
    }

    protected virtual void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = _direction * bulletSpeed;
    } 

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Player":
                collision.gameObject.GetComponent<Player>().DoDamage(bulletDamage);
                break;
            case "Enemy":
                collision.gameObject.GetComponent<Enemy>().DoDamage(bulletDamage);
                break;
        }

        BulletManager.AddBullet(gameObject);
        if (collision.gameObject.tag != "Bullet")
            Destroy(this);
    }
}
