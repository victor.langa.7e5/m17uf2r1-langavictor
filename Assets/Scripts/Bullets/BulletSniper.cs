using UnityEngine;

public class BulletSniper : Bullet
{
    private float _distanceTravelled;

    void Update()
    {
        _distanceTravelled += Time.deltaTime * bulletSpeed;
        if (_distanceTravelled > 50)
            Destroy(gameObject);
    }

    protected override void Start()
    {
        base.Start();
        foreach (RaycastHit2D ray in Physics2D.RaycastAll(transform.position, _direction))
        {
            switch (ray.collider.gameObject.tag)
            {
                case "Player":
                    ray.collider.gameObject.GetComponent<Player>().DoDamage(bulletDamage);
                    break;
                case "Enemy":
                    ray.collider.gameObject.GetComponent<Enemy>().DoDamage(bulletDamage);
                    break;
                case "DestructibleObject":
                    Destroy(ray.collider.gameObject);
                    break;
            }

            if (ray.collider.attachedRigidbody != null)
                ray.collider.attachedRigidbody.AddForce(_direction * bulletSpeed);
        }
    }
}
