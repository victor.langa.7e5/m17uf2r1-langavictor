using UnityEngine;

public class BulletGrenade : Bullet
{
    ExplodeWhenEntityNearby _explodeWhenEntityNearby;

    protected override void Start()
    {
        base.Start();
        _explodeWhenEntityNearby = GetComponent<ExplodeWhenEntityNearby>();
    }

    private void Update()
    {
        if (_explodeWhenEntityNearby.GetExplosionState() == ExplodeWhenEntityNearby.ExplosionState.Done)
            Destroy(gameObject);
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {}
}
