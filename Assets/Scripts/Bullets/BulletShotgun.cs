using UnityEngine;

public class BulletShotgun : Bullet
{
    protected override void Start()
    {
        float angleDispersion = 45 * Mathf.Deg2Rad;
        float delta = Random.Range(-angleDispersion, angleDispersion);
        _direction = new Vector2(
             _direction.x * Mathf.Cos(delta) - _direction.y * Mathf.Sin(delta),
             _direction.x * Mathf.Sin(delta) + _direction.y * Mathf.Cos(delta));
        base.Start();
    }
}
