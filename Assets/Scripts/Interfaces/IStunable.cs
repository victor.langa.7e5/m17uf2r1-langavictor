public interface IStunable
{
    enum StunStateEnum
    {
        NotStunned,
        Stunned,
        InvulnerableToStun
    }

    StunStateEnum StunState { get; }
    float StunTime { get; set; }

    public bool IsStunned();

    public void ApplyStun(float stunTime);   

    public void EndStun();
}
