using UnityEngine;

public interface IDropable
{
    public GameObject[] DropableItems { get; set; }

    public GameObject ReturnItemToDrop() => DropableItems[Random.Range(0, DropableItems.Length)];
}
