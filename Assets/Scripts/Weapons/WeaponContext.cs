using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class WeaponContext : MonoBehaviour
{
    public WeaponListSO weaponList;

    public enum EnumWeapons
    { 
        Pistol,
        Shotgun,
        AssaultRifle,
        SniperRifle,
        GrenadeLauncher,
        StunFlamethrower
    }

    [HideInInspector] public Weapon weapon;

    private EnumWeapons _weaponEnum;
    private int _currentStateIndex;

    void Start()
    {
        _currentStateIndex = 0;

        if (gameObject.transform.parent.CompareTag("Player"))
            _weaponEnum = EnumWeapons.Pistol;
        else
        {
            _currentStateIndex = Random.Range(0, Enum.GetValues(typeof(EnumWeapons)).Length); 
            ChangeWeaponEnum();
        }

        TransitionToState();
    }

    void Update()
    {
        if (gameObject.transform.parent.CompareTag("Player") && weapon.GetCanShoot())
        {
            if (Input.GetButtonDown("NextWeapon") || Input.mouseScrollDelta.y < 0)
            {
                SetNextStateIndex();
                HandleState();
            }

            if (Input.GetButtonDown("PreviousWeapon") || Input.mouseScrollDelta.y > 0)
            {
                SetPreviousStateIndex();
                HandleState();
            } 
        }

        weapon.UpdateWeaponValues();
    }

    void HandleState()
    {
        RegisterStateData();
        ChangeWeaponEnum();
        TransitionToState();
    }

    void ChangeWeaponEnum()
    {
        switch (_currentStateIndex)
        { 
            case 0:
                _weaponEnum = EnumWeapons.Pistol;
                break;
            case 1:
                _weaponEnum = EnumWeapons.Shotgun;
                break;
            case 2:
                _weaponEnum = EnumWeapons.AssaultRifle;
                break;
            case 3:
                _weaponEnum = EnumWeapons.SniperRifle;
                break;
            case 4:
                _weaponEnum = EnumWeapons.GrenadeLauncher;
                break;
            case 5:
                _weaponEnum = EnumWeapons.StunFlamethrower;
                break;
        }
    }

    void SetNextStateIndex() => _currentStateIndex = (_currentStateIndex + 1) % Enum.GetValues(typeof(EnumWeapons)).Length;

    void SetPreviousStateIndex() =>_currentStateIndex = _currentStateIndex == 0 ? Enum.GetValues(typeof(EnumWeapons)).Length - 1 : _currentStateIndex - 1;

    void TransitionToState()
    {
        switch (_weaponEnum)
        { 
            case EnumWeapons.Pistol:
                weapon = gameObject.AddComponent<Pistol>();
                break;
            case EnumWeapons.Shotgun:
                weapon = gameObject.AddComponent<Shotgun>();
                break;
            case EnumWeapons.AssaultRifle:
                weapon = gameObject.AddComponent<AssaultRifle>();
                break;
            case EnumWeapons.SniperRifle:
                weapon = gameObject.AddComponent<SniperRifle>();
                break;
            case EnumWeapons.GrenadeLauncher:
                weapon = gameObject.AddComponent<GrenadeLauncher>();
                break;
            case EnumWeapons.StunFlamethrower:
                weapon = gameObject.AddComponent<StunFlamethrower>();
                break;
        }

        weapon.SetWeaponInitialValues();
        weapon.SetValues(this);
    }

    void RegisterStateData()
    {
        weapon.RegisterValues(this);
        Destroy(weapon);
    }

    public EnumWeapons GetCurrentState() => _weaponEnum;
}
