using UnityEngine;

public class Pistol : Weapon
{
    public override void SetValues(WeaponContext weaponContext)
    {
        bullet = weaponContext.weaponList.pistolValues.bullet;

        reloadTime = weaponContext.weaponList.pistolValues.reloadTime;
        timeBetweenShots = weaponContext.weaponList.pistolValues.timeBetweenShots;

        bulletsStored = weaponContext.weaponList.pistolValues.bulletsStored;
        totalBullets = weaponContext.weaponList.pistolValues.totalBullets;
        bulletsLeft = weaponContext.weaponList.pistolValues.bulletsLeft;
    }

    override public void RegisterValues(WeaponContext weaponContext)
    {
        if (character.CompareTag("Player"))
        {
            weaponContext.weaponList.pistolValues.bullet = bullet;

            weaponContext.weaponList.pistolValues.reloadTime = reloadTime;
            weaponContext.weaponList.pistolValues.timeBetweenShots = timeBetweenShots;

            weaponContext.weaponList.pistolValues.bulletsStored = bulletsStored;
            weaponContext.weaponList.pistolValues.totalBullets = totalBullets;
            weaponContext.weaponList.pistolValues.bulletsLeft = bulletsLeft;
        }
    }
}
