using UnityEngine;

public class StunFlamethrower : Weapon
{
    Collider2D _collider;
    RaycastHit2D[] hits = new RaycastHit2D[300];

    private float _timeToStun;

    private void Start() => _collider = GetComponent<PolygonCollider2D>();

    public override void SetValues(WeaponContext weaponContext)
    {
        _timeToStun = weaponContext.weaponList.stunFlamethrowerValues.timeToStun;

        bullet = weaponContext.weaponList.stunFlamethrowerValues.bullet;

        reloadTime = weaponContext.weaponList.stunFlamethrowerValues.reloadTime;
        timeBetweenShots = weaponContext.weaponList.stunFlamethrowerValues.timeBetweenShots;

        bulletsStored = weaponContext.weaponList.stunFlamethrowerValues.bulletsStored;
        totalBullets = weaponContext.weaponList.stunFlamethrowerValues.totalBullets;
        bulletsLeft = weaponContext.weaponList.stunFlamethrowerValues.bulletsLeft;
    }

    override public void RegisterValues(WeaponContext weaponContext)
    {
        if (character.CompareTag("Player"))
        {
            weaponContext.weaponList.stunFlamethrowerValues.timeToStun = _timeToStun;

            weaponContext.weaponList.stunFlamethrowerValues.bullet = bullet;

            weaponContext.weaponList.stunFlamethrowerValues.reloadTime = reloadTime;
            weaponContext.weaponList.stunFlamethrowerValues.timeBetweenShots = timeBetweenShots;

            weaponContext.weaponList.stunFlamethrowerValues.bulletsStored = bulletsStored;
            weaponContext.weaponList.stunFlamethrowerValues.totalBullets = totalBullets;
            weaponContext.weaponList.stunFlamethrowerValues.bulletsLeft = bulletsLeft;
        }
    }

    protected override void Shoot()
    {
        int totalHits = _collider.Cast(Camera.main.ScreenToWorldPoint(Input.mousePosition) - GameManager.Instance.GetPlayerPosition(), hits, 1);

        for (int i = 0; i < totalHits; i++)
        {
            if (hits[i].collider.gameObject.TryGetComponent(out IStunable stunable))
                stunable.ApplyStun(_timeToStun);
        }

        hits = new RaycastHit2D[300];
        bulletsLeft--;
    }
}
