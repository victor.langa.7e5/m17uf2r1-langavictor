using System.Collections;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    protected GameObject bullet;
    [HideInInspector] public Character character;
    
    protected Aim _aim;

    private bool _isAutonomousShoot;

    protected float reloadTime;
    protected int bulletsStored;
    protected int totalBullets;
    protected int bulletsLeft;
    
    protected float timeBetweenShots;
    protected float currentTimeAfterShoot;
    public bool canShoot;

    public void SetWeaponInitialValues()
    {
        character = transform.parent.CompareTag("Player") ? transform.parent.GetComponent<Player>() : transform.parent.GetComponent<Enemy>();

        _aim = GetComponent<Aim>();

        _isAutonomousShoot = transform.parent.CompareTag("Enemy");

        currentTimeAfterShoot = timeBetweenShots;
        canShoot = true;
    }

    public virtual void UpdateWeaponValues()
    {
        _aim.AimAt(character.GetAimTarget());
        if (canShoot)
        {
            currentTimeAfterShoot += Time.deltaTime;
            if ((_isAutonomousShoot ? true : Input.GetButton("Shoot")) && currentTimeAfterShoot >= timeBetweenShots && bulletsLeft > 0)
                Shoot();
            if ((_isAutonomousShoot ? bulletsLeft <= 0 : (Input.GetButtonDown("Reload") && bulletsStored > 0)) && bulletsLeft < totalBullets)
                StartCoroutine(ReloadWeapon());
        }
    }

    protected virtual void Shoot()
    {
        bulletsLeft--;
        Instantiate(bullet, transform.position, transform.rotation, transform);
        currentTimeAfterShoot = 0;
    }

    protected virtual IEnumerator ReloadWeapon()
    {
        canShoot = false;
        yield return new WaitForSecondsRealtime(reloadTime);

        if (_isAutonomousShoot) bulletsLeft = totalBullets;
        else 
        {
            bulletsStored += bulletsLeft;
            bulletsLeft = Mathf.Clamp(totalBullets, 0, bulletsStored);
            bulletsStored -= bulletsLeft;
        }

        currentTimeAfterShoot = timeBetweenShots;
        canShoot = true;
    }

    public virtual void SetValues(WeaponContext weaponContext) { }

    public virtual void RegisterValues(WeaponContext weaponContext) { }

    public bool GetCanShoot() => canShoot;

    public int GetBulletsLeft() => bulletsLeft;

    public int GetBulletsStored() => bulletsStored;

    public int AddBulletsStored(int bulletsToAdd) => bulletsStored += bulletsToAdd;
}
