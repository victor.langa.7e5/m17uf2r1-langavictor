public class SniperRifle : Weapon
{
    public override void SetValues(WeaponContext weaponContext)
    {
        bullet = weaponContext.weaponList.sniperRifleValues.bullet;

        reloadTime = weaponContext.weaponList.sniperRifleValues.reloadTime;
        timeBetweenShots = weaponContext.weaponList.sniperRifleValues.timeBetweenShots;

        bulletsStored = weaponContext.weaponList.sniperRifleValues.bulletsStored;
        totalBullets = weaponContext.weaponList.sniperRifleValues.totalBullets;
        bulletsLeft = weaponContext.weaponList.sniperRifleValues.bulletsLeft;
    }

    override public void RegisterValues(WeaponContext weaponContext)
    {
        if (character.CompareTag("Player"))
        {
            weaponContext.weaponList.sniperRifleValues.bullet = bullet;

            weaponContext.weaponList.sniperRifleValues.reloadTime = reloadTime;
            weaponContext.weaponList.sniperRifleValues.timeBetweenShots = timeBetweenShots;

            weaponContext.weaponList.sniperRifleValues.bulletsStored = bulletsStored;
            weaponContext.weaponList.sniperRifleValues.totalBullets = totalBullets;
            weaponContext.weaponList.sniperRifleValues.bulletsLeft = bulletsLeft;
        }
    }
}
