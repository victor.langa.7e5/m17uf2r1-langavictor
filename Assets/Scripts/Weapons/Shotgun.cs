public class Shotgun : Weapon
{
    private int _bulletsPerShot;

    public override void SetValues(WeaponContext weaponContext)
    {
        _bulletsPerShot = weaponContext.weaponList.shotgunValues.bulletsPerShot;

        bullet = weaponContext.weaponList.shotgunValues.bullet;

        reloadTime = weaponContext.weaponList.shotgunValues.reloadTime;
        timeBetweenShots = weaponContext.weaponList.shotgunValues.timeBetweenShots;

        bulletsStored = weaponContext.weaponList.shotgunValues.bulletsStored;
        totalBullets = weaponContext.weaponList.shotgunValues.totalBullets;
        bulletsLeft = weaponContext.weaponList.shotgunValues.bulletsLeft;
    }

    override public void RegisterValues(WeaponContext weaponContext)
    {
        if (character.CompareTag("Player"))
        {
            weaponContext.weaponList.shotgunValues.bulletsPerShot = _bulletsPerShot;

            weaponContext.weaponList.shotgunValues.bullet = bullet;

            weaponContext.weaponList.shotgunValues.reloadTime = reloadTime;
            weaponContext.weaponList.shotgunValues.timeBetweenShots = timeBetweenShots;

            weaponContext.weaponList.shotgunValues.bulletsStored = bulletsStored;
            weaponContext.weaponList.shotgunValues.totalBullets = totalBullets;
            weaponContext.weaponList.shotgunValues.bulletsLeft = bulletsLeft;
        }
    }

    protected override void Shoot()
    {
        for (int i = 0; i < _bulletsPerShot && bulletsLeft > 0; i++)
            base.Shoot();
    }
}
