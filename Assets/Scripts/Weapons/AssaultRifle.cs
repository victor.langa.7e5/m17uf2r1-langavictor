using UnityEngine;

public class AssaultRifle : Weapon
{
    public override void SetValues(WeaponContext weaponContext)
    {
        bullet = weaponContext.weaponList.assaultRifleValues.bullet;

        reloadTime = weaponContext.weaponList.assaultRifleValues.reloadTime;
        timeBetweenShots = weaponContext.weaponList.assaultRifleValues.timeBetweenShots;

        bulletsStored = weaponContext.weaponList.assaultRifleValues.bulletsStored;
        totalBullets = weaponContext.weaponList.assaultRifleValues.totalBullets;
        bulletsLeft = weaponContext.weaponList.assaultRifleValues.bulletsLeft;
    }

    override public void RegisterValues(WeaponContext weaponContext)
    {
        if (character.CompareTag("Player"))
        {
            weaponContext.weaponList.assaultRifleValues.bullet = bullet;

            weaponContext.weaponList.assaultRifleValues.reloadTime = reloadTime;
            weaponContext.weaponList.assaultRifleValues.timeBetweenShots = timeBetweenShots;

            weaponContext.weaponList.assaultRifleValues.bulletsStored = bulletsStored;
            weaponContext.weaponList.assaultRifleValues.totalBullets = totalBullets;
            weaponContext.weaponList.assaultRifleValues.bulletsLeft = bulletsLeft;
        }
    }
}
