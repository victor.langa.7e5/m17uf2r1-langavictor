public class GrenadeLauncher : Weapon
{
    public override void SetValues(WeaponContext weaponContext)
    {
        bullet = weaponContext.weaponList.grenadeLauncherValues.bullet;

        reloadTime = weaponContext.weaponList.grenadeLauncherValues.reloadTime;
        timeBetweenShots = weaponContext.weaponList.grenadeLauncherValues.timeBetweenShots;

        bulletsStored = weaponContext.weaponList.grenadeLauncherValues.bulletsStored;
        totalBullets = weaponContext.weaponList.grenadeLauncherValues.totalBullets;
        bulletsLeft = weaponContext.weaponList.grenadeLauncherValues.bulletsLeft;
    }

    override public void RegisterValues(WeaponContext weaponContext)
    {
        if (character.CompareTag("Player"))
        {
            weaponContext.weaponList.grenadeLauncherValues.bullet = bullet;

            weaponContext.weaponList.grenadeLauncherValues.reloadTime = reloadTime;
            weaponContext.weaponList.grenadeLauncherValues.timeBetweenShots = timeBetweenShots;

            weaponContext.weaponList.grenadeLauncherValues.bulletsStored = bulletsStored;
            weaponContext.weaponList.grenadeLauncherValues.totalBullets = totalBullets;
            weaponContext.weaponList.grenadeLauncherValues.bulletsLeft = bulletsLeft;
        }
    }
}
