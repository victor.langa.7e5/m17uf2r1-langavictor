using TMPro;
using UnityEngine;

public class PlayerPrefsUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _bestScoreText;
    [SerializeField] string _bestScoreAnnounce;

    [Space]
    [SerializeField] TextMeshProUGUI _enemiesKilledText;
    [SerializeField] string _enemiesKilledAnnounce;

    [Space]
    [SerializeField] TextMeshProUGUI _roomsSurpassedText;
    [SerializeField] string _roomsSurpassedAnnounce;

    [Space]
    [SerializeField] TextMeshProUGUI _lastScore0Text;
    [SerializeField] string _lastScore0Announce;

    [Space]
    [SerializeField] TextMeshProUGUI _lastScore1Text;
    [SerializeField] string _lastScore1Announce;

    [Space]
    [SerializeField] TextMeshProUGUI _lastScore2Text;
    [SerializeField] string _lastScore2Announce;

    [Space]
    [SerializeField] TextMeshProUGUI _resultText;

    private void Start()
    {
        if (_bestScoreText != null)
            _bestScoreText.text = _bestScoreAnnounce + PlayerPrefs.GetInt("bestScore", 0).ToString();

        if (_enemiesKilledText != null)
            _enemiesKilledText.text = _enemiesKilledAnnounce + PlayerPrefs.GetInt("enemiesKilled", 0).ToString();

        if (_roomsSurpassedText != null)
            _roomsSurpassedText.text = _roomsSurpassedAnnounce + PlayerPrefs.GetInt("roomsSurpassed", 0).ToString();

        if (_lastScore0Text != null)
            _lastScore0Text.text = _lastScore0Announce + PlayerPrefs.GetInt("lastScore0", 0).ToString();

        if (_lastScore1Text != null)
            _lastScore1Text.text = _lastScore1Announce + PlayerPrefs.GetInt("lastScore1", 0).ToString();

        if (_lastScore2Text != null)
            _lastScore2Text.text = _lastScore2Announce + PlayerPrefs.GetInt("lastScore2", 0).ToString();

        if (_resultText != null)
            _resultText.text = PlayerPrefs.GetInt("result") == 0 ? "Victory" : "Defeat";
    }
}
