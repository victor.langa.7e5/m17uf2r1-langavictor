using TMPro;
using UnityEngine;

public class WeaponData : MonoBehaviour
{
    [SerializeField] private WeaponContext _weaponContext;

    [SerializeField] private TextMeshProUGUI _bulletsLeftText;
    [SerializeField] private TextMeshProUGUI _bulletsStoredText;
    [SerializeField] private TextMeshProUGUI _weaponNameText;

    void LateUpdate() => UpdateUIWeaponValues();

    void UpdateUIWeaponValues()
    {
        _bulletsLeftText.text = _weaponContext.weapon.GetBulletsLeft().ToString();
        _bulletsStoredText.text = _weaponContext.weapon.GetBulletsStored().ToString();
        _weaponNameText.text = _weaponContext.GetCurrentState().ToString();
    }
}
