using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    [SerializeField] private Image _transitionImage;
    private Color _imageColor;
    private float _alpha;

    private void Start()
    {
        _transitionImage.color = _imageColor;
        StartCoroutine(DoFading(true));
    }

    public IEnumerator DoFading(bool isFadeIn)
    {
        if (isFadeIn)
        {
            _alpha = 1;
            while (_alpha > 0)
            {
                _alpha -= 0.25f * Time.deltaTime;
                _imageColor.a = _alpha;
                _transitionImage.color = _imageColor;
                yield return new WaitForEndOfFrame();
            }

            _transitionImage.enabled = false;
        }
        else
        {
            _alpha = 0;
            _transitionImage.enabled = true;
            while (_alpha < 1)
            {
                _alpha += 0.25f * Time.deltaTime;
                _imageColor.a = _alpha;
                _transitionImage.color = _imageColor;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
