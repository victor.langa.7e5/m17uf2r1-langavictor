using UnityEngine;

public class Healthbar : MonoBehaviour
{
    [SerializeField] private Character _character;

    [SerializeField] private RectTransform _currentHpLeftRT;
    [SerializeField] private RectTransform _differenceHpRT;

    private float _initialHpWidth;

    void Start() => _initialHpWidth = _currentHpLeftRT.sizeDelta.x;

    void Update()
    {
        UpdateHpLeft();
        UpdateDifferenceHp();
    }

    void UpdateHpLeft()
    {
        _currentHpLeftRT.sizeDelta = new Vector2(
            Mathf.Round(_character.GetHpLeft() * _initialHpWidth / _character.GetMaxHp() * 100) / 100,
            _currentHpLeftRT.sizeDelta.y
        );
    }

    void UpdateDifferenceHp()
    {
        if (_currentHpLeftRT.sizeDelta.x == _differenceHpRT.sizeDelta.x)
            return;
        else if (_currentHpLeftRT.sizeDelta.x > _differenceHpRT.sizeDelta.x)
            _differenceHpRT.sizeDelta = _currentHpLeftRT.sizeDelta;
        else
            _differenceHpRT.sizeDelta = new Vector2(
                _differenceHpRT.sizeDelta.x - (Time.deltaTime * 100),
                _differenceHpRT.sizeDelta.y
            );
    }
}
