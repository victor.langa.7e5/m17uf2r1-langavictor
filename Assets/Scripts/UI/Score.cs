using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText;

    [SerializeField] private int _pointsPerSecond;
    private float _scoreShown;

    void Start()
    {
        _scoreShown = GameManager.Instance.GetScore();
        _scoreText.text = _scoreShown.ToString("0000000000000.");
    }

    void Update()
    {
        if (Mathf.Abs(GameManager.Instance.GetScore() - _scoreShown) > 1)
        {
            if (_scoreShown < GameManager.Instance.GetScore())
                _scoreShown += Time.deltaTime * _pointsPerSecond;
            else if (_scoreShown > GameManager.Instance.GetScore())
                _scoreShown = Mathf.Clamp(_scoreShown - Time.deltaTime * _pointsPerSecond, 0, _scoreShown);
        }

        _scoreText.text = _scoreShown.ToString("0000000000000.");
    }
}
