using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] RectTransform _settingsPanelRectTransform;

    [SerializeField] AudioMixer _audioMixer;
    [SerializeField] Slider _generalVolumeSlider;
    [SerializeField] Slider _musicVolumeSlider;
    [SerializeField] Slider _soundEffectsVolumeSlider;

    Coroutine _currentCoroutine = default;

    private void Start()
    {
        _audioMixer.GetFloat("masterVolume", out float masterVolume);
        _audioMixer.GetFloat("musicVolume", out float musicVolume);
        _audioMixer.GetFloat("soundEffectsVolume", out float soundEffectsVolume);

        _generalVolumeSlider.value = (masterVolume + 20) / 30;
        _musicVolumeSlider.value = (musicVolume + 20) / 30;
        _soundEffectsVolumeSlider.value = (soundEffectsVolume + 30) / 30;
    }

    private void OnEnable()
    {
        if (_settingsPanelRectTransform != null)
        {
            TryStopPanelMovement();
            Time.timeScale = 0;
            _currentCoroutine = StartCoroutine(MoveSettingPanel(true));
        }
    }

    void Update()
    {
        _audioMixer.SetFloat("masterVolume", (_generalVolumeSlider.value * 30) - 20);
        _audioMixer.SetFloat("musicVolume", (_musicVolumeSlider.value * 30) - 20);
        _audioMixer.SetFloat("soundEffectsVolume", (_soundEffectsVolumeSlider.value * 30) - 30);
    }

    private void OnDisable()
    {
        if (_settingsPanelRectTransform != null)
        {
            TryStopPanelMovement();
            _currentCoroutine = StartCoroutine(MoveSettingPanel(false));
        }
    }

    IEnumerator MoveSettingPanel(bool isAppearing)
    {
        while (isAppearing ? _settingsPanelRectTransform.anchoredPosition.y <= _settingsPanelRectTransform.rect.height / 2 : _settingsPanelRectTransform.anchoredPosition.y >= _settingsPanelRectTransform.rect.height / -2)
        {
            _settingsPanelRectTransform.anchoredPosition += new Vector2(0, _settingsPanelRectTransform.rect.height * Time.unscaledDeltaTime) * (isAppearing ? 1 : -1);
            yield return new WaitForEndOfFrame();
        }

        if (!isAppearing)
            Time.timeScale = 1;

        _currentCoroutine = default;
    }

    bool TryStopPanelMovement()
    {
        if (_currentCoroutine != default)
        {
            StopCoroutine(_currentCoroutine);
            _currentCoroutine = default;
            return true;
        }

        return false;
    }
}
