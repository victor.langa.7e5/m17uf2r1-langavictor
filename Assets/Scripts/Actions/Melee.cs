using UnityEngine;

public class Melee : MonoBehaviour
{
    private float _timeLeftToMeleeAvailable;

    void Start() => _timeLeftToMeleeAvailable = 0;

    void Update() => _timeLeftToMeleeAvailable -= Time.deltaTime;

    public void MeleeAttack(Vector2 startPoint, Vector2 aimPoint, Vector2 size, float damage, float range, float force, float cooldown)
    {
        if (Input.GetButtonDown("Melee") && _timeLeftToMeleeAvailable <= 0)
        {
            Vector2 direction = aimPoint - (Vector2)transform.position;
            direction.Normalize();

            Debug.DrawRay(startPoint, direction, Color.red, range);
            foreach (RaycastHit2D ray in Physics2D.BoxCastAll(startPoint, size, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg, direction, range))
            {
                if (ray.collider.gameObject.CompareTag("Enemy"))
                {
                    Vector2 forceVector = ray.collider.transform.position - transform.position;
                    forceVector.Normalize();

                    if (ray.collider.attachedRigidbody != null)
                        ray.collider.attachedRigidbody.AddForce(forceVector * force);

                    ray.collider.GetComponent<Enemy>().DoDamage(damage);
                }
            }

            _timeLeftToMeleeAvailable = cooldown;
        }
    }
}
