using System.Collections;
using UnityEngine;

public class ExplodeWhenEntityNearby : MonoBehaviour
{
    public enum ExplosionState
    { 
        Available,
        InProgress,
        Done,
        Cooldown
    }

    [SerializeField] private ExplosionSO _explosionSO;

    private ExplosionState _enumExplosionState;

    private float _explosionForce;
    private float _explosionDamage;

    private float _minimumDistanceToExplode;
    private float _explosionRadius;

    private float _spareSeconds;
    private float _explosionCooldown;

    void Start()
    {
        _enumExplosionState = ExplosionState.Available;

        _explosionForce = _explosionSO.explosionForce;
        _explosionDamage = _explosionSO.explosionDamage;

        _minimumDistanceToExplode = _explosionSO.minimumDistanceToExplode;
        _explosionRadius = _explosionSO.explosionRadius;

        _spareSeconds = _explosionSO.spareSeconds;
        _explosionCooldown = _explosionSO.explosionCooldown;
    }

    void Update()
    {
        switch (_enumExplosionState)
        {
            case ExplosionState.Available:
                if ((GameManager.Instance.GetPlayerPosition() - transform.position).magnitude < _minimumDistanceToExplode)
                    StartCoroutine(Explode());
                break;
            case ExplosionState.Done:
                StartCoroutine(StartExplosionCooldown());
                break;
        }
        
    }

    IEnumerator Explode()
    {
        _enumExplosionState = ExplosionState.InProgress;

        float timer = 0;
        while (timer < _spareSeconds)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        foreach (RaycastHit2D ray in Physics2D.CircleCastAll(transform.position, _explosionRadius, Vector2.zero))
            if (ray.collider.gameObject.CompareTag("Player") || ray.collider.gameObject.CompareTag("Enemy"))
            {
                if (ray.collider.gameObject != gameObject)
                {
                    switch (ray.collider.gameObject.tag)
                    {
                        case "Player":
                            ray.collider.gameObject.GetComponent<Player>().DoDamage(_explosionDamage);
                            break;
                        case "Enemy":
                            ray.collider.gameObject.GetComponent<Enemy>().DoDamage(_explosionDamage);
                            break;
                        case "DestructibleObject":
                            Destroy(ray.collider.gameObject);
                            break;
                    }

                    if (ray.collider.attachedRigidbody != null)
                        ray.collider.attachedRigidbody.AddForce((ray.collider.transform.position - transform.position) * _explosionForce);
                }
            }

        _enumExplosionState = ExplosionState.Done;
    }

    IEnumerator StartExplosionCooldown()
    {
        _enumExplosionState = ExplosionState.Cooldown;

        if (gameObject.tag == "Enemy")
            GetComponent<Movement>().SetIfEnemyIsChasing(false);

        float timer = 0;
        while (timer < _explosionCooldown)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }        

        if (gameObject.tag == "Enemy")
            GetComponent<Movement>().SetIfEnemyIsChasing(true);

        _enumExplosionState = ExplosionState.Available;
    }

    public ExplosionState GetExplosionState() => _enumExplosionState;
}
