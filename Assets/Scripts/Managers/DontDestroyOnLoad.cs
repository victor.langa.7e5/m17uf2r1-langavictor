using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    private void Awake() => DontDestroyOnLoad(gameObject);
    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("currentScore", 0);
        PlayerPrefs.SetInt("roomsSurpassed", 0);
        PlayerPrefs.SetInt("enemiesKilled", 0);
        PlayerPrefs.SetInt("currentFloor", 1);
        PlayerPrefs.Save();
    }
}
