using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    private Transform _playerTransform;
    [SerializeField] private WeaponListSO _standardWeaponList;
    [SerializeField] private Settings _settingsUI;

    private int _score;
    [SerializeField] private int _scoreToSubstractEachSecond;
    [SerializeField] private int _standarizedScoreBaseValue; // I think this was like a multiplier for the score of each level, but I will not implement it now

    private int _currentFloorRoomsSurpassed;
    private int _roomsSurpassed;
    public int enemiesKilled;

    private int _currentFloor;
    public int CurrentFloor 
    {
        get => _currentFloor;
    }

    [SerializeField] AudioClip _hotlineMiamiMusic;
    [SerializeField] AudioClip _cyberpunkMusic;
    [SerializeField] AudioClip _bayonettaMusic;

    void Awake()
    {
        if (_instance != this && _instance != null) Destroy(this);
        _instance = this;

        _playerTransform = GameObject.FindWithTag("Player").transform;

        _currentFloor = PlayerPrefs.GetInt("currentFloor", 1);

        var gameAudioSource = Camera.main.gameObject.GetComponent<AudioSource>();
        switch (_currentFloor)
        {
            case 1:
                gameAudioSource.clip = _hotlineMiamiMusic;
                break;
            case 2:
                gameAudioSource.clip = _cyberpunkMusic;
                break;
            case 3:
                gameAudioSource.clip = _bayonettaMusic;
                break;
        }
        gameAudioSource.Play();

        _score = PlayerPrefs.GetInt("currentScore", 0);

        _currentFloorRoomsSurpassed = 0;
        _roomsSurpassed = PlayerPrefs.GetInt("roomsSurpassed", 0);
        enemiesKilled = PlayerPrefs.GetInt("enemiesKilled", 0);
    }

    private void Start() => StartCoroutine(EachSecondSubstractScore());

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            switch (_settingsUI.enabled)
            {
                case true:
                    _settingsUI.enabled = false;
                    break;
                case false:
                    _settingsUI.enabled = true;
                    break;
            }
    }

    private IEnumerator EachSecondSubstractScore()
    { 
        yield return new WaitForSeconds(1);
        _score = Mathf.Clamp(_score - _scoreToSubstractEachSecond, 0, _score);
        StartCoroutine(EachSecondSubstractScore());
    }

    public Vector3 GetPlayerPosition() => _playerTransform.position;

    public void AddScore(int scoreToAdd) => _score += scoreToAdd;

    public int GetScore() => _score;

    public void AddWaveScore() => AddScore((_standarizedScoreBaseValue + UnityEngine.Random.Range(_standarizedScoreBaseValue - _standarizedScoreBaseValue / 2, _standarizedScoreBaseValue + _standarizedScoreBaseValue / 2)) * _currentFloor);

    public void AddEndRoomScore() => AddScore(_standarizedScoreBaseValue * UnityEngine.Random.Range(2, 5));

    public void GoToNextLevel() => StartCoroutine(SetNextLevel());

    public IEnumerator SetNextLevel()
    {
        PlayerPrefs.SetInt("currentFloor", ++_currentFloor);
        PlayerPrefs.SetInt("currentScore", _score);

        if (_currentFloor > 3)
            StartCoroutine(EndGame(true));
        else
        {
            yield return gameObject.GetComponent<SceneTransition>().DoFading(false);
            ProjectSceneManager.LoadGame();
        }
    }

    public void RestartGame()
    {
        ResetPlayerWeaponsSOValues();
        _roomsSurpassed = 0;
        enemiesKilled = 0;
        // SceneManager.LoadScene(0); 
    }

    void ResetPlayerWeaponsSOValues()
    {
        WeaponListSO playerWeaponList = _playerTransform.gameObject.GetComponentInChildren<WeaponContext>().weaponList;

        playerWeaponList.pistolValues.SetValues(_standardWeaponList.pistolValues);
        playerWeaponList.assaultRifleValues.SetValues(_standardWeaponList.assaultRifleValues);
        playerWeaponList.shotgunValues.SetValues(_standardWeaponList.shotgunValues);
        playerWeaponList.grenadeLauncherValues.SetValues(_standardWeaponList.grenadeLauncherValues);
        playerWeaponList.sniperRifleValues.SetValues(_standardWeaponList.sniperRifleValues);
    }

    public IEnumerator EndGame(bool isVictory)
    {
        DisablePlayerControls();

        PlayerPrefs.SetInt("enemiesKilled", enemiesKilled);
        PlayerPrefs.SetInt("roomsSurpassed", _roomsSurpassed);
        PlayerPrefs.SetInt("currentFloor", 1);
        SetScorePlayerPrefsValues();

        if (isVictory)
            PlayerPrefs.SetInt("result", 0);
        else
            PlayerPrefs.SetInt("result", 1);

        yield return gameObject.GetComponent<SceneTransition>().DoFading(false);

        ProjectSceneManager.LoadDefeat();
    }

    void SetScorePlayerPrefsValues()
    {
        if (PlayerPrefs.GetInt("bestScore", 0) < _score)
            PlayerPrefs.SetInt("bestScore", _score);

        PlayerPrefs.SetInt("lastScore2", PlayerPrefs.GetInt("lastScore1", 0));
        PlayerPrefs.SetInt("lastScore1", PlayerPrefs.GetInt("lastScore0", 0));
        PlayerPrefs.SetInt("lastScore0", _score);
    }

    void EnablePlayerControls() => ChangePlayerControlComponentsStatus(true);

    void DisablePlayerControls() => ChangePlayerControlComponentsStatus(false);

    void ChangePlayerControlComponentsStatus(bool status)
    {
        GameObject player = _playerTransform.gameObject;

        player.GetComponent<Player>().enabled = status;
        player.GetComponent<Movement>().enabled = status;
        player.GetComponent<Dash>().enabled = status;
        player.GetComponent<Melee>().enabled = status;
        player.transform.GetChild(0).gameObject.GetComponent<WeaponContext>().enabled = false;
    }

    public void AddRoomSurpassed() 
    {
        _roomsSurpassed++;
        _currentFloorRoomsSurpassed++;
        if (_currentFloorRoomsSurpassed >= LevelsSpawner.Instance.GetRoomsSpawned())
            GoToNextLevel();
    }
}
