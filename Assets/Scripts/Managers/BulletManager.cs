using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    private const int MaxBulletsStored = 100;
    private static Queue<GameObject> _bulletQueue = new Queue<GameObject>();

    public static void AddBullet(GameObject bullet) 
    {
        _bulletQueue.Enqueue(bullet);
        if (_bulletQueue.Count >= MaxBulletsStored)
            Destroy(_bulletQueue.Dequeue());
    }
}
