using UnityEngine;
using UnityEngine.SceneManagement;

public class ProjectSceneManager : MonoBehaviour
{
    public static void LoadMenu() => SceneManager.LoadSceneAsync(0);

    public static void LoadGame() => SceneManager.LoadSceneAsync(1);

    public static void LoadGameNotAsync() => SceneManager.LoadScene(1);

    public static void LoadDefeat() => SceneManager.LoadSceneAsync(2);
    public static void ExitGame() => Application.Quit();
}
