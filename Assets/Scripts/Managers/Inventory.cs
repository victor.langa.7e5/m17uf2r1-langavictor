using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    BuyableItemSO _buyableItemSO;
    [SerializeField] Image _itemImage;
    [SerializeField] TextMeshProUGUI _itemText;

    private void Start() => DeleteItem();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
            UseItem();
    }

    public void AddItem(BuyableItemSO buyableItemSO)
    { 
        _buyableItemSO = buyableItemSO;
        GameManager.Instance.AddScore(-_buyableItemSO.cost);
        _itemImage.sprite = _buyableItemSO.image;
        _itemText.text = _buyableItemSO.itemName;
    }

    void DeleteItem()
    {
        _itemImage.sprite = null;
        _itemText.text = "No item";
    }

    void UseItem()
    {
        if (_buyableItemSO == null)
            return;

        _buyableItemSO.ItemAction();
        DeleteItem();
    }
}
