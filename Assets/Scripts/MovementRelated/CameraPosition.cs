using UnityEngine;

public class CameraPosition : MonoBehaviour
{
    void FixedUpdate()
    {
        Vector2 cameraPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition) - GameManager.Instance.GetPlayerPosition();
        if (cameraPosition.magnitude > 1)
            cameraPosition.Normalize();
        Camera.main.transform.position = GameManager.Instance.GetPlayerPosition() + new Vector3(cameraPosition.x, cameraPosition.y, Camera.main.transform.position.z);
    }
}
