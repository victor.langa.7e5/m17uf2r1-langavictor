using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (_camera.fieldOfView == 155)
                _camera.fieldOfView = 170;
            else _camera.fieldOfView = 155;
        }
        _camera.transform.position = new Vector3(GameManager.Instance.GetPlayerPosition().x, GameManager.Instance.GetPlayerPosition().y, _camera.transform.position.z);
    }
}
