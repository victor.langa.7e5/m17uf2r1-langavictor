using UnityEngine;

public class Dash : MonoBehaviour
{
    private Rigidbody2D _rb;

    private float _timeLeftToDashAvailable;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();

        _timeLeftToDashAvailable = 0;
    }

    void Update() => _timeLeftToDashAvailable -= Time.deltaTime;

    public bool TryDash(Vector3 forceDirection, int force, float cooldown)
    {
        if (Input.GetButtonDown("Dash") && _timeLeftToDashAvailable <= 0 && (forceDirection.x != 0 || forceDirection.y != 0))
        {
            forceDirection.Normalize();
            _rb.AddForce(forceDirection * force);
            _timeLeftToDashAvailable = cooldown;
            return true;
        }

        return false;
    }
}
