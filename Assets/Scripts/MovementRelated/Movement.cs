using UnityEngine;

public class Movement : MonoBehaviour
{
    public bool isControlled;
    private bool _isEnemyChasing; // this is only used if the character moving is an enemy

    private Character _character;
    private Rigidbody2D _objectRigidbody;

    private float[] _inputValues = new float[2];

    void Start()
    {
        _isEnemyChasing = true;
        _character = gameObject.tag == "Player" ? GetComponent<Player>() : GetComponent<Enemy>();
        _objectRigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        _inputValues[0] = Input.GetAxis("Horizontal");
        _inputValues[1] = Input.GetAxis("Vertical");
    }

    void FixedUpdate()
    {
        _objectRigidbody.velocity += new Vector2(
            Time.deltaTime * _character.GetSpeed() * (isControlled ? _inputValues[0] : GetPlayerPositionRelativeDirection()[0]),
            Time.deltaTime * _character.GetSpeed() * (isControlled ? _inputValues[1] : GetPlayerPositionRelativeDirection()[1]));

        _objectRigidbody.velocity = new Vector2(
            Mathf.Clamp(_objectRigidbody.velocity.x, -_character.GetSpeed(), + _character.GetSpeed()),
            Mathf.Clamp(_objectRigidbody.velocity.y, -_character.GetSpeed(), +_character.GetSpeed()));
    }


    // The methods down there are only used when the character moving is an enemy

    public void SetIfEnemyIsChasing(bool isChasing) => _isEnemyChasing = isChasing;

    float[] GetPlayerPositionRelativeDirection()
    {
        return new float[] 
        { 
            (_character.GetAimTarget().x - transform.position.x) * (_isEnemyChasing ? 1 : -1), 
            (_character.GetAimTarget().y - transform.position.y) * (_isEnemyChasing ? 1 : -1)
        };
    }
}
