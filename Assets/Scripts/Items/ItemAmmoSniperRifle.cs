using UnityEngine;

public class ItemAmmoSniperRifle : ItemAmmo
{
    void Start() => weapon = WeaponContext.EnumWeapons.SniperRifle;

    protected override void DoEffect(Collider2D collision)
    {
        weaponContext = collision.transform.GetChild(0).GetComponent<WeaponContext>();
        if (weaponContext.GetCurrentState() == weapon)
            weaponContext.weapon.AddBulletsStored(weaponContext.weaponList.sniperRifleValues.totalBullets);
        else
            weaponContext.weaponList.sniperRifleValues.bulletsStored += weaponContext.weaponList.sniperRifleValues.totalBullets;
    }
}
