using TMPro;
using UnityEngine;

public class ShopableItems : MonoBehaviour
{
    [SerializeField] Inventory _inventory;
    [SerializeField] BuyableItemSO _buyableItemSO;

    [SerializeField] GameObject _shopableItemPanel;
    [SerializeField] TextMeshProUGUI _shopableItemText;

    private void Start() => GetComponent<SpriteRenderer>().sprite = _buyableItemSO.image;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _shopableItemPanel.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, 2));
            ChangeItemShopPanelState(true);
            _shopableItemText.text = $"Item: {_buyableItemSO.itemName}\n\nDescription: {_buyableItemSO.description}\n\nCost: {_buyableItemSO.cost} score";
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _shopableItemPanel.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, 2));
            if (Input.GetKey(KeyCode.E))
                BuyItem();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            ChangeItemShopPanelState(false);
    }

    void ChangeItemShopPanelState(bool isAppearing)
    {
        if (isAppearing)
            _shopableItemPanel.SetActive(true);
        else 
            _shopableItemPanel.SetActive(false);
    }

    void BuyItem()
    {
        if (GameManager.Instance.GetScore() >= _buyableItemSO.cost)
        {
            _inventory.AddItem(_buyableItemSO);
            Destroy(gameObject);
        }
    }
}
