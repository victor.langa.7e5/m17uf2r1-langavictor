using UnityEngine;

public class ItemAmmoAssaultRifle : ItemAmmo
{
    void Start() => weapon = WeaponContext.EnumWeapons.AssaultRifle;

    protected override void DoEffect(Collider2D collision)
    {
        weaponContext = collision.transform.GetChild(0).GetComponent<WeaponContext>();
        if (weaponContext.GetCurrentState() == weapon)
            weaponContext.weapon.AddBulletsStored(weaponContext.weaponList.assaultRifleValues.totalBullets);
        else
            weaponContext.weaponList.assaultRifleValues.bulletsStored += weaponContext.weaponList.assaultRifleValues.totalBullets;
    }
}
