using static WeaponContext;

public abstract class ItemAmmo : ConsumableItem
{
    protected EnumWeapons weapon;
    protected WeaponContext weaponContext;
}
