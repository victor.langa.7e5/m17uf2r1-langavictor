using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAmmoStunFlamethrower : ItemAmmo
{
    void Start() => weapon = WeaponContext.EnumWeapons.StunFlamethrower;

    protected override void DoEffect(Collider2D collision)
    {
        weaponContext = collision.transform.GetChild(0).GetComponent<WeaponContext>();
        if (weaponContext.GetCurrentState() == weapon)
            weaponContext.weapon.AddBulletsStored(weaponContext.weaponList.stunFlamethrowerValues.totalBullets);
        else
            weaponContext.weaponList.stunFlamethrowerValues.bulletsStored += weaponContext.weaponList.stunFlamethrowerValues.totalBullets;
    }
}
