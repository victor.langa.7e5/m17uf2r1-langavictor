using UnityEngine;

public class ItemHealth : ConsumableItem
{
    [SerializeField] private float _hpToHeal;

    protected override void DoEffect(Collider2D collision) => collision.gameObject.GetComponent<Player>().HealHp(_hpToHeal);
}
