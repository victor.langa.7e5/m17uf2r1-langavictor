using UnityEngine;

public class ItemAmmoShotgun : ItemAmmo
{
    void Start() => weapon = WeaponContext.EnumWeapons.Shotgun;

    protected override void DoEffect(Collider2D collision)
    {
        weaponContext = collision.transform.GetChild(0).GetComponent<WeaponContext>();
        if (weaponContext.GetCurrentState() == weapon)
            weaponContext.weapon.AddBulletsStored(weaponContext.weaponList.shotgunValues.totalBullets);
        else
            weaponContext.weaponList.shotgunValues.bulletsStored += weaponContext.weaponList.shotgunValues.totalBullets;
    }
}
