using UnityEngine;

public abstract class ConsumableItem : MonoBehaviour
{
    private void Update()
    {
        if ((GameManager.Instance.GetPlayerPosition() - transform.position).magnitude < 3)
            transform.position = Vector3.MoveTowards(transform.position, GameManager.Instance.GetPlayerPosition(), Time.deltaTime * 2);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DoEffect(collision);
            Destroy(gameObject);
        }
    }

    protected abstract void DoEffect(Collider2D collision);
}
