using UnityEngine;

public class ItemAmmoGrenadeLauncher : ItemAmmo
{
    void Start() => weapon = WeaponContext.EnumWeapons.GrenadeLauncher;

    protected override void DoEffect(Collider2D collision)
    {
        weaponContext = collision.transform.GetChild(0).GetComponent<WeaponContext>();
        if (weaponContext.GetCurrentState() == weapon)
            weaponContext.weapon.AddBulletsStored(weaponContext.weaponList.grenadeLauncherValues.totalBullets);
        else
            weaponContext.weaponList.grenadeLauncherValues.bulletsStored += weaponContext.weaponList.grenadeLauncherValues.totalBullets;
    }
}
